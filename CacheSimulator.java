
//Cache Simulator
//Alexander Glavin() and Vipul Bhat(vbhat1@jhu.edu)
//Computer Systems Fundamentals (601.229)
//HW 6

import java.lang.Math;
import java.lang.Integer;
import java.util.Scanner;
import java.io.FileReader;
import java.io.IOException;
import java.io.BufferedReader;
import java.util.List;
import java.util.ArrayList;

public class CacheSimulator {

    private static final long MAX_32BIT = ((long) 1) << 32;

    private static int numSets = -1;
    private static int numBlocks = -1;
    private static int numBytes = -1;
    private static boolean wAllo = false;
    private static boolean wThro = false;
    private static boolean lru = false;
    private static String filename;

    private static int loads = 0;
    private static int stores = 0;
    private static int loadHits = 0;
    private static int loadMisses = 0;
    private static int storeHits = 0;
    private static int storeMisses = 0;
    private static int cycles = 0;

    private static List<List<Block>> cacheData;

    public static final class Block {
	boolean dirtyBit;
	int tag;
	boolean valid;
	long time;

	Block() {
	    dirtyBit = false;
	    valid = false;
            time = 0;
	    tag = -1;
        }
    }

    public static void main(String[] args) {
	if (!processArguments(args)) {
            return;
        }
	if (readFile()) {
	    printResults();
	}
    }

    public static void printResults() {
	System.out.printf("Total loads: %d\n", loads);
        System.out.printf("Total stores: %d\n", stores);
        System.out.printf("Load hits: %d\n", loadHits);
        System.out.printf("Load misses: %d\n", loadMisses);
        System.out.printf("Store hits: %d\n", storeHits);
        System.out.printf("Store misses: %d\n", storeMisses);
        System.out.printf("Total cycles: %d\n", cycles);
    }

    public static boolean processArguments(String[] args) {
	if (args.length != 7) {
	    System.err.println("Please enter the correct 7 command line arguments");
	    return false;
	}
	int writeAllocate = -1, writeThrough = -1, leastRecentlyUsed = -1;
	try {
	    numSets = Integer.parseInt(args[0]);
	    numBlocks = Integer.parseInt(args[1]);
	    numBytes = Integer.parseInt(args[2]);
	    writeAllocate = Integer.parseInt(args[3]);
	    writeThrough = Integer.parseInt(args[4]);
	    leastRecentlyUsed = Integer.parseInt(args[5]);
	} catch (NumberFormatException e) {
	    System.err.println("First 6 command line arguments must be integers");
	    return false;
	}
	if (!powerOfTwo(numSets) || !powerOfTwo(numBlocks) || !powerOfTwo(numBytes) || numBytes < 4) {
	    System.err.println("First 3 arguments(number of sets in the cache, number of blocks in each set, and number of bytes in each block) must be positive powers of 2 and third argument(number of bytes in each block) must also be at least 4");
	    return false;
	}
	if (writeAllocate != 1 && writeAllocate != 0) {
	    System.err.println("4th argument must be 1 for write-allocate or 0 for not write-allocate");
	    return false;
	}
	if (writeThrough != 1 && writeThrough != 0) {
	    System.err.println("5th argument must be 1 for write-through or 0 for write-back");
	    return false;
	}
	if (leastRecentlyUsed != 1 && leastRecentlyUsed != 0) {
	    System.err.println("6th argument must be 1 for least-recently-used or 0 for FIFO evictions");
	    return false;
	}
	if (writeAllocate == 0 && writeThrough == 0) {
            System.err.println("Cannot have write allocate and write through both set to zero");
	    return false;
        }
	filename = args[6];
	lru = (leastRecentlyUsed == 1);
	wAllo = (writeAllocate == 1);
	wThro = (writeThrough == 1);
        return true;
    }

    public static boolean readFile() {
	try {
	    BufferedReader reader = new BufferedReader(new FileReader(filename));
	    initializeCache();
	    String line = "";
            while ((line = reader.readLine()) != null) {
                if (!processLine(line)) {
                    // maybe change to skip line
		    return false;
		}
            }
	} catch(IOException e) {
	    System.err.println("Please enter valid filename as the 7th argument");
	    return false;
	}
	return true;
    }

    public static void initializeCache() {
	cacheData = new ArrayList<>(numSets);
	for (int i = 0; i < numSets; i++) {
	    List<Block> temp = new ArrayList<>(numBlocks);
	    for (int j = 0; j < numBlocks; j++) {
                Block tempBlock = new Block();
		temp.add(j, tempBlock);
            }
	    cacheData.add(i, temp);
        }
    }

    public static boolean processLine(String line) {
	String[] parts = line.split("\\s+");
        if (parts.length != 3) {
            System.err.println("Must have 3 fields per line");
            return false;
        }
        if (!parts[0].equals("s") && !parts[0].equals("l")) {
            System.err.println("First argument on line must be s or l");
            return false;
        }
        if (parts[1].length() < 3 || !parts[1].substring(0,2).equals("0x")) {
            System.err.println("Line does not contain properly formatted hexadecimal number");
            return false;
        }
        long address = 0;
        try {
            address = Long.parseLong(parts[1].substring(2), 16);
        } catch (NumberFormatException e) {
            System.err.println("Line does not contain propely formatted hexadecimal number");
            return false;
        }
        if (address >= MAX_32BIT || address < 0) {
            System.err.println("Address is larger than 32-bits");
            return false;
        }
	boolean load = parts[0].equals("l");
	if (load) {
	    loadCache(address);
        } else {
	    storeCache(address);
	}
	return true;
    }

    public static void loadCache(long address) {
        int tag = getTag(address);
	int index = getIndex(address);
	int blkIndex = getBlockIndex(index, tag);
	if (blkIndex == -1) {
	    loadMiss(index, tag);
        } else {
            loadHit(index, tag, blkIndex);
        }
    }

    public static void storeCache(long address) {
        int tag = getTag(address);
        int index = getIndex(address);
        int blkIndex = getBlockIndex(index, tag);
	if (blkIndex == -1) {
            storeMiss(index, tag);
        } else {
            storeHit(index, tag, blkIndex);
        }
    }

    public static int missFindReplacement(int index) {
        if (numBlocks != 1) {
            for (int i = 0; i < numBlocks; i++) {
                if (!cacheData.get(index).get(i).valid || cacheData.get(index).get(i).time == (numBlocks - 1)) {
                    return i;
                }
            }
        }
	return 0;
    }

    public static void loadHit(int index, int tag, int blkIndex) {
        if (numBlocks != 1 && lru) {
            lruUpdateTime(index, blkIndex);
        }
        cycles++;
        loadHits++;
        loads++;
    }


    public static void loadMiss(int index, int tag) {
        int slot = missFindReplacement(index);
        if (!wThro && cacheData.get(index).get(slot).valid && cacheData.get(index).get(slot).dirtyBit) {
            cacheData.get(index).get(slot).dirtyBit = false;
            cycles += 25 * numBytes;
        }
        cacheData.get(index).get(slot).valid = true;
        cacheData.get(index).get(slot).tag = tag;
        if (numBlocks != 1) {
            cacheData.get(index).get(slot).time = -1;
            incrementAllTime(index);
        }
	cycles += 25 * numBytes;
	cycles++;
        loadMisses++;
	loads++;
    }

    public static void storeMiss(int index, int tag) {
	if (wAllo) {
	    int slot = missFindReplacement(index);
	    cycles += 25 * numBytes;
	    if (wThro) {
		cycles += 25 * numBytes;
		cycles++;
            } else {
                if (cacheData.get(index).get(slot).valid && cacheData.get(index).get(slot).dirtyBit) {
                    cycles += 25 * numBytes;
                }
                cycles++;
		cacheData.get(index).get(slot).dirtyBit = true;
            }
            if (numBlocks != 1) {
                cacheData.get(index).get(slot).time = -1;
	        incrementAllTime(index);
            }
            cacheData.get(index).get(slot).valid = true;
            cacheData.get(index).get(slot).tag = tag;
        } else {
	    cycles += 25 * numBytes;
        }
	storeMisses++;
	stores++;

    }

    public static void storeHit(int index, int tag, int blkIndex) {
	if (wThro) {
	    cycles += 25 * numBytes;
	    cycles++;
        } else {
	    cycles++;
            cacheData.get(index).get(blkIndex).dirtyBit = true;
        }
	if (numBlocks != 1 && lru) {
            lruUpdateTime(index, blkIndex);
        }
        stores++;
	storeHits++;
    }


    public static void lruUpdateTime(int index, int slot) {
       long time = cacheData.get(index).get(slot).time;
       if (time == 0) {
           return;
       }
       for (int i = 0; i < numBlocks; i++) {
            if (cacheData.get(index).get(i).time < time) {
                cacheData.get(index).get(i).time++;
            } else if (cacheData.get(index).get(i).time == time) {
	        cacheData.get(index).get(i).time = 0;
            }
        }
    }

    public static void incrementAllTime(int index) {
        for (int i = 0; i < numBlocks; i++) {
            cacheData.get(index).get(i).time++;
        }
    }

    public static int getBlockIndex(int index, int tag) {
        for (int i = 0; i < numBlocks; i++) {
	    if (cacheData.get(index).get(i).valid && cacheData.get(index).get(i).tag == tag) {
                return i;
            }
        }
	return -1;
    }

    public static int getTag(long address) {
        int offsetBits = calcPowerOfTwo(numBytes);
        int indexBits = calcPowerOfTwo(numSets);
        int tagCompare = 0xffffffff >> (offsetBits + indexBits) << (offsetBits + indexBits);
	int tag = (int) ((address & tagCompare) >> (offsetBits + indexBits));
	return tag;
   }

    public static int getIndex(long address) {
        int offsetBits = calcPowerOfTwo(numBytes);
        int index = (int) ((address >> offsetBits) & (numSets - 1));
	return index;
    }


    public static boolean powerOfTwo(int n) {
	if (n < 1) {
	    return false;
	}
	while (n != 1) {
	    if (n % 2 != 0) {
		return false;
	    }
	    n /= 2;
	}
	return true;
    }

    public static int calcPowerOfTwo(int n) {
	int count = 0;
	while (n != 1) {
	    n /= 2;
	    count++;
	}
	return count;
    }

}
