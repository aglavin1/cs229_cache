CFLAGS=-std=gnu11 -Wall -Wextra -pedantic -O2 -g

all: timeit matrix

clean:
	rm -rf timeit matrix
